﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CEF.ApiAuthJWT.Infrastructure.Model;
using CEF.ApiAuthJWT.Infrastucture.IClient;
using CEF.ApiAuthJWT.Infrastucture.Model.Request;
using CEF.ApiAuthJWT.Infrastucture.Model.Response;
using CEF.ServicesAuth.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CEF.ServicesAuth.IServices;
using Newtonsoft.Json;

namespace CEF.ApiAuthJWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InterfaceCEFServicesController : ControllerBase
    {
          internal readonly ConfigureEndPointServicesCEF _ObjEndPoint;
        internal ILog _Logg;
        internal readonly CEFConfigurationApiAuth _Configuration;
        internal IClientServiceCEF _Cliente;
        public InterfaceCEFServicesController(CEFConfigurationApiAuth Configuration , ConfigureEndPointServicesCEF ObjEndPoint, IClientServiceCEF Cliente, ILog Logg)
        {
              _ObjEndPoint = ObjEndPoint;
            _Configuration = Configuration;
            _Cliente = Cliente;
            _Logg = Logg;
        }
        [HttpPost]
         
            public async Task<ActionResult> Sync(CEFRequest value)
        {
            try
            {
                CEFResponse ObjResupuesta = await _Cliente.Sync(value);
                await _Logg.LogAddInfo("Call Success Request:"+ JsonConvert.SerializeObject(value));
                return Ok(ObjResupuesta);
            }catch (Exception ex)
            {
                await _Logg.LogAddError(ex.Message.ToString());
                return BadRequest(ex.Message.ToString());
            }



    }
    }
}
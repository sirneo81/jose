﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.Model;
using CEF.ServicesAuth.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CEF.ApiAuthJWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JwtAuthController : ControllerBase
    {
        internal TokenManager manageToken;
        internal IRepositoryWrapper _repo;
        internal CEFConfigurationApiAuth _Configuration;
        public JwtAuthController(TokenManager _mt, IRepositoryWrapper repo,CEFConfigurationApiAuth Configuration)
        {
            manageToken = _mt;
            _repo = repo;
            _Configuration = Configuration;
        }

        [HttpGet("authentification/{code}")]
        public async Task<ActionResult > Get(string code)
        {
            ApplicationKey ObjAppManager = new ApplicationKey(_repo);
            AppToken ObjApp = await ObjAppManager.GetTokenFromAppKeyCode(code);
            if(ObjApp.Actived && ObjApp.IdApp>0)
            {

                return Ok(ObjApp.Token);
            
            }
            else
            {
                return Unauthorized("no access app");
            }
           
        }
        [HttpGet("refreshToken/{code}")]
        public async Task<ActionResult> refreshToken(string code)
        {
            ApplicationKey ObjAppManager = new ApplicationKey(_repo);
            AppToken ObjApp = await ObjAppManager.GetTokenFromAppKeyCode(code);
            if (ObjApp.Actived && ObjApp.IdApp > 0)
            {
                var claims = new List<Claim>
                    {
                    new Claim(ClaimTypes.Name, ObjApp.Name )
                    };
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ObjApp.IdApp.ToString ()));
                claims.Add(new Claim(ClaimTypes.Role, AppRoles.Read));
                string token = await manageToken.GenerateTokenJwt(ObjApp.Name, _Configuration, claims);
                ObjApp.Token = token;
                await ObjAppManager.SaveTokenToApp(ObjApp);
                return Ok(token);

            }
            else
            {
                return Unauthorized("no access app");
            }

        }

    }
}
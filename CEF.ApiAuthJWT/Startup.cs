﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using CEF.ApiAuthJWT.Infrastructure;
using CEF.ApiAuthJWT.Infrastructure.Model;
using CEF.ApiAuthJWT.Infrastucture.IClient;
using CEF.ApiAuthJWT.Infrastucture.Model;
using CEF.EFAuthJwt.EF;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.IServices;
using CEF.ServicesAuth.Model;
using CEF.ServicesAuth.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace CEF.ApiAuthJWT
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(CEFConfigurationApiAuth), typeof(CEFConfigurationApiAuth));
            services.AddSingleton(typeof(LogConfigurationFile), typeof(LogConfigurationFile));
            CEFConfigurationApiAuth ObjConfiguracion = new CEFConfigurationApiAuth(Configuration);
            
            services.AddDbContext<EFDataContext>(
             options => options.UseSqlServer(ObjConfiguracion.ConnectionStringDB));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient(typeof(IRepositoryWrapper), typeof(RepositoryWrapper));
            services.AddTransient(typeof(TokenManager), typeof(TokenManager));
            services.AddTransient(typeof(ConfigureEndPointServicesCEF), typeof(ConfigureEndPointServicesCEF));
            services.AddTransient(typeof(ILog), typeof(LogFile));
            ConfigureEndPointServicesCEF ObjConfiguracionEndPoint = new ConfigureEndPointServicesCEF(Configuration);
            services.AddHttpClient<IClientServiceCEF, ClientServiceCEF>( "ServiceCef",c=>{
                c.BaseAddress = new Uri(ObjConfiguracionEndPoint.URI_BASE);
                c.Timeout = new TimeSpan(0, 0, 10);
            });
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                Convert.FromBase64String(ObjConfiguracion.JWT_SECRET_KEY)
            );

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)

           .AddJwtBearer(options =>
           {
               options.SaveToken = true;
               options.TokenValidationParameters = new TokenValidationParameters
               {

                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = false,
                   ValidIssuer = ObjConfiguracion.JWT_ISSUER_TOKEN,
                   ValidAudience = ObjConfiguracion.JWT_AUDIENCE_TOKEN,
                   IssuerSigningKey = _symmetricSecurityKey
               };
               IdentityModelEventSource.ShowPII = true;
               options.Events = new JwtBearerEvents
               {
                    
                   OnAuthenticationFailed = context =>
                   {
                       var invalidAudienceException = context.Exception as SecurityTokenInvalidAudienceException;
                       
                       context.Fail(context.Exception.Message.ToString());
                        
                       if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                       {
                           context.Response.WriteAsync("Token-Expired");
                       }
                       else
                       {
                           context.Response.WriteAsync("invalid_token:" + context.Exception.Message.ToString());
                       }
                       return Task.CompletedTask;
                   }
               };

           });

            services.AddAuthorization();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(ApiConstants.Swagger.ApiVersion, new Info
                {
                    Version = ApiConstants.Swagger.ApiVersion,
                    Title = ApiConstants.Swagger.ApiName,
                    Description = ApiConstants.Swagger.ApiName,
                    TermsOfService = "None"
                });

                c.IgnoreObsoleteProperties();
                c.IgnoreObsoleteActions();
                c.DescribeAllParametersInCamelCase();
                c.DescribeStringEnumsInCamelCase();
                c.DescribeAllEnumsAsStrings();


                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // c.IncludeXmlComments(xmlPath);

                c.AddSecurityDefinition(ApiConstants.Bearer, new ApiKeyScheme()
                {
                    In = "header",
                    Description = "Enter the JWT Token. If you do not have it run GenerateToken API. Introduce Bearer before the jwt followed by a white space, eg: **Bearer &lt;yourjwttoken&gt;**",
                    Name = ApiConstants.Authorization,
                    Type = "apiKey"
                });

                var securityRequirements = new Dictionary<string, IEnumerable<string>> {
                    { ApiConstants.Bearer, Enumerable.Empty<string>() }
                };
                c.AddSecurityRequirement(securityRequirements);
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.Host = httpReq.Host.Value);
              //  c.RouteTemplate = ApiConstants.Swagger.RouteTemplate;
            })
                .UseSwaggerUI(c =>
                {
                    string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                    c.DocumentTitle = ApiConstants.Swagger.ApiName;
                    c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", $"{ApiConstants.Swagger.ApiName} {ApiConstants.Swagger.ApiVersion}");
                  //  c.RoutePrefix = ApiConstants.Swagger.RoutePrefix;

                });

            app.UseCors(builder => builder.AllowAnyOrigin()
                             .AllowAnyMethod()
                             .AllowAnyHeader().WithExposedHeaders("Token-Expired"));
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseHandlerRequestMiddleware();
            app.UseMvc();
        }
    }
}

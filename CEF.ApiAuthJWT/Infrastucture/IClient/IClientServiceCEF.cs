﻿using CEF.ApiAuthJWT.Infrastucture.Model.Request;
using CEF.ApiAuthJWT.Infrastucture.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.ApiAuthJWT.Infrastucture.IClient
{
    public interface IClientServiceCEF
    {
          Task<CEFResponse> Sync(CEFRequest request);
    }
}

﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.ApiAuthJWT.Infrastructure
{
    public static class ExtensionHandlerRequestMiddleware
    {
        public static IApplicationBuilder UseHandlerRequestMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HandlerHttpRequestMiddelware>();
        }
    }
}

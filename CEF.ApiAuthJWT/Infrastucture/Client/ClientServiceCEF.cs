﻿using CEF.ApiAuthJWT.Infrastucture.IClient;
using CEF.ApiAuthJWT.Infrastucture.Model.Request;
using CEF.ApiAuthJWT.Infrastucture.Model.Response;
using CEF.ApiAuthJWT.Infrastucture.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CEF.ApiAuthJWT.Infrastructure.Model;

namespace CEF.ApiAuthJWT.Infrastucture.IClient
{
    public class ClientServiceCEF: IClientServiceCEF
    {
        public HttpClient Client { get; }

        public ClientServiceCEF()
        { }
        private readonly HttpClient client;
        private readonly ConfigureEndPointServicesCEF _ObjUrIConf;
        public ClientServiceCEF(HttpClient client, ConfigureEndPointServicesCEF ObjUriConf)
        {
            this.client = client;
            _ObjUrIConf = ObjUriConf;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            Client = client;
        }

        public async Task<CEFResponse> Sync(CEFRequest request)
        {
             var builder = new UriBuilder(_ObjUrIConf.URI_BASE+"/"+ _ObjUrIConf.SYNC_FUNCTION);
            string strUri = builder.ToString();
            CEFResponse ObjRespuesta = await SendGetAsync<CEFRequest>(Client, strUri, request);
               return ObjRespuesta;
        }
        private async Task<CEFResponse> SendGetAsync<T>(HttpClient client, string requestUri, CEFRequest request)
        {
            CEFResponse ObjRespuesta = new CEFResponse();
            try
            {
                CancellationToken cancellationToken;

                
                using (var orequest = new HttpRequestMessage(HttpMethod.Post, requestUri))
                {
                    var json = JsonConvert.SerializeObject(request);
                    using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        orequest.Content = stringContent;

                        var response = await client
                            .SendAsync(orequest, HttpCompletionOption.ResponseHeadersRead);
                        if (!response.IsSuccessStatusCode)
                        {
                            string userInfo = await response.Content.ReadAsStringAsync();
                          
                        }
                        else
                        
                            {
                            response.EnsureSuccessStatusCode();
                            string result = await response.Content.ReadAsStringAsync();
                            ObjRespuesta = JsonConvert.DeserializeObject<CEFResponse>(result);
                        }
                    }
                }
            }catch( Exception ex)
            {
                throw ex;
            }
            return ObjRespuesta;

        }
    }
}

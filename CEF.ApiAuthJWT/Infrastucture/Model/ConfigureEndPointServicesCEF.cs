﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.ApiAuthJWT.Infrastructure.Model
{
    public class ConfigureEndPointServicesCEF
    {
        public string URI_BASE = "";
        public string SYNC_FUNCTION = "";
    
        public ConfigureEndPointServicesCEF(IConfiguration Configuracion)
        {
            URI_BASE = Configuracion.GetValue<string>("AppEndPoingServiceUri");
            SYNC_FUNCTION = Configuracion.GetValue<string>("AppEndPoingServiceFunction");
           
        }
        public ConfigureEndPointServicesCEF() { }
    }
}

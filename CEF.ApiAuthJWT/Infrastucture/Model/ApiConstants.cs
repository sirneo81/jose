﻿public static partial class ApiConstants
{
    /// <summary>
    /// Api version header
    /// </summary>
    public static string ApiVersionHeader => "x-api-version";
    /// <summary>
    /// Api key header
    /// </summary>
    public static string ApiKeyHeader => "x-api-key";
    /// <summary>
    /// Guid key header
    /// </summary>
    public static string GuidHeader => "x-guid";
    /// <summary>
    /// Customer Id key header
    /// </summary>
    public static string CustomerIdHeader => "x-customerId";
    /// <summary>
    /// Bearer
    /// </summary>
    public static string Bearer => "Bearer";
    /// <summary>
    /// Authentication
    /// </summary>
    public static string Authorization => "Authorization";
    public static string AuthorizationApp => "app-auth";
    /// <summary>
    /// Api key
    /// </summary>
    public static string ApiKey => "ApiKey";
    public static class Swagger
    {
        /// <summary>
        /// Swagger endpoint
        /// </summary>
        public static string Endpoint => "/api-docs/v1/swagger.json";
        /// <summary>
        /// Swagger Api Name
        /// </summary>
        public static string ApiName => "Api CEF Jwt";
        /// <summary>
        /// Swagger Api Version
        /// </summary>
        public static string ApiVersion => "v1";
        /// <summary>
        /// Swagger route
        /// </summary>
        public static string RouteTemplate => "api-docs/{documentName}/swagger.json";
        /// <summary>
        /// Swagger route prefix
        /// </summary>
        public static string RoutePrefix => "swagger";
    }
}
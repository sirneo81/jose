﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CEF.AdminApiApp.Infrastructure;
using CEF.AdminApiApp.Models;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAppAdminApi.Model;
using CEF.ServicesAppAdminApi.Services;
using CEF.Utils.Security;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace CEF.AdminApiApp.Controllers
{
    public class LoginController : Controller
    {
        internal IRepositoryWrapper _repo;
        internal ConfigurationTokenJWT _Configuration;

        public LoginController(IRepositoryWrapper Repo, ConfigurationTokenJWT Configuration)
        {
            _repo = Repo;
            _Configuration = Configuration;


        }
        public async Task<IActionResult> Index()
        {
            LoginModel ObjLogin = new LoginModel();
             
            return View(ObjLogin);
        }
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction ("Index");
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(LoginModel ObjLogin)
        {
            try
            {
                if(await  new Users(_repo).VerifyUser(ObjLogin.User , ObjLogin.Password))
                {
                  UserApp ObjUsuario= await  new Users(_repo).GetUserByUserName(ObjLogin.User);
                    var claims = new List<Claim>
                    {
                    new Claim(ClaimTypes.Name, ObjLogin.User )
                    };
                    if (ObjUsuario == null)
                    {
                        ModelState.AddModelError("", "username or password is invalid");
                    }
                    else
                    {

                        claims.Add(new Claim("hasAccess","true"));
                        //claims.Add(new Claim(ClaimTypes.Role, CustomRoles.Admin));
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, ObjUsuario.Id.ToString()));
                        string token = UtilyJwt.GenerateTokenJwt(ObjUsuario.Name, _Configuration.JWT_SECRET_KEY, _Configuration.JWT_AUDIENCE_TOKEN,
                            _Configuration.JWT_ISSUER_TOKEN, _Configuration.JWT_EXPIRE_MINUTES, claims);
                        claims.Add(new Claim("access_token", token));
                        var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                        ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                        var authticationProperties = new AuthenticationProperties();

                        await HttpContext.SignInAsync(
                          CookieAuthenticationDefaults.AuthenticationScheme,
                          new ClaimsPrincipal(userIdentity),
                          authticationProperties);

                        HttpContext.Session.SetString("JWToken", token);

                        

                            return RedirectToAction ("Index","Home");
                        


                    }
                }
                else
                {
                    ModelState.AddModelError("", "Usuario o Clave Incorrectos");
                }
                return View(  ObjLogin);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message.ToString()+" "+ _Configuration.ConnectionStringDB);
                return View();
            }
        }

    }
}
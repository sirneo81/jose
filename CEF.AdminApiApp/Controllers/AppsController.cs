﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using CEF.AdminApiApp.Infrastructure;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.Model;
using CEF.ServicesAuth.Services;
using CEF.Utils.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CEF.AdminApiApp.Controllers
{
    [Authorize]
    public class AppsController : Controller
    {
        internal IRepositoryWrapper _repo;
        internal ConfigurationTokenJWT _Configuration;

        public AppsController(IRepositoryWrapper Repo, ConfigurationTokenJWT Configuration)
        {
            _repo = Repo;
            _Configuration = Configuration;


        }
        public async Task<IActionResult> Index()
        {
            List<AppToken> ObjApps =await new ApplicationKey(_repo).GetApps();
            return View(ObjApps);
        }
        public async Task<IActionResult> Edit(int Id)
        {
            AppToken ObjApp = new AppToken();
            ObjApp = await new ApplicationKey(_repo).GetTokenFromAppKey(Id);
            return View(ObjApp);
        }
        //public async Task<IActionResult> Edit(int Id)
        //{
        //    AppToken ObjApp = new AppToken();
        //    ObjApp = await new ApplicationKey(_repo).GetTokenFromAppKey(Id);
        //    return View(ObjApp);
        //}
         
        [ValidateAntiForgeryToken]
        [HttpPost]
       
        public async Task<IActionResult> Save(AppToken oApp,string appsAcction)
        {
            if (appsAcction== "Generar Token")
            {
                AppToken ObjApp = oApp;
                var claims = new List<Claim>
                        {
                        new Claim(ClaimTypes.Name, ObjApp.Name )
                        };
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, ObjApp.IdApp.ToString()));
                        claims.Add(new Claim(ClaimTypes.Role, AppRoles.Read));
                        string token = UtilyJwt.GenerateTokenJwt(ObjApp.Name, _Configuration.JWT_SECRET_KEY, _Configuration.JWT_AUDIENCE_TOKEN,
                                _Configuration.JWT_ISSUER_TOKEN, _Configuration.JWT_EXPIRE_MINUTES, claims);
                        ObjApp.Token = token;
                await new ApplicationKey(_repo).SaveTokenToApp(ObjApp);
            }
            else if (appsAcction == "Close")
            {
                return RedirectToAction("Index", new {  });
            }
            else
            { 
            
                if (oApp.IdApp ==0)
                {
                    AppToken ObjApp = oApp;
                    var key = new byte[32];
                    using (var generator = RandomNumberGenerator.Create())
                    generator.GetBytes(key);
                    string apiKey = Convert.ToBase64String(key);
                    var claims = new List<Claim>
                        {
                        new Claim(ClaimTypes.Name, ObjApp.Name )
                        };
                        claims.Add(new Claim(ClaimTypes.NameIdentifier, ObjApp.IdApp.ToString()));
                        claims.Add(new Claim(ClaimTypes.Role, AppRoles.Read));
                        string token = UtilyJwt.GenerateTokenJwt(ObjApp.Name, _Configuration.JWT_SECRET_KEY, _Configuration.JWT_AUDIENCE_TOKEN,
                                _Configuration.JWT_ISSUER_TOKEN, _Configuration.JWT_EXPIRE_MINUTES, claims);
                        ObjApp.Token = token;
                        ObjApp.Code = apiKey;
                        await new ApplicationKey(_repo).SaveTokenToApp(ObjApp);
                        oApp.IdApp= ObjApp.IdApp;
                        oApp.Token = ObjApp.Token;


                }
                else
                {
                    AppToken ObjApp = await new ApplicationKey(_repo).GetTokenFromAppKey(oApp.IdApp );
                    ObjApp.Actived = oApp.Actived;
                    ObjApp.IdApp = oApp.IdApp;
                    ObjApp.Name = oApp.Name;
                    
                    await new ApplicationKey(_repo).SaveTokenToApp(ObjApp);


                }
            }
            return RedirectToAction("Edit",new { Id = oApp.IdApp });
        }
    }
}
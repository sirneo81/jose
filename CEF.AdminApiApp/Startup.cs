﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
 
using CEF.AdminApiApp.Infrastructure;
using CEF.EFAuthJwt.EF;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.IServices;
using CEF.ServicesAuth.Model;
using CEF.ServicesAuth.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace CEF.AdminApiApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(ConfigurationTokenJWT), typeof(ConfigurationTokenJWT));
            services.AddSingleton(typeof(LogConfigurationFile), typeof(LogConfigurationFile));
            ConfigurationTokenJWT ObjConfiguracion = new ConfigurationTokenJWT(Configuration);
            
            services.AddDbContext<EFDataContext>(
             options => options.UseSqlServer(ObjConfiguracion.ConnectionStringDB));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient(typeof(IRepositoryWrapper), typeof(RepositoryWrapper));
             

            services.AddSession(options =>
            {

                options.Cookie.IsEssential = true;
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
         
            services.AddAuthentication(o =>
            {
                o.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                o.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                o.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {

                options.Cookie.Name = "CEF.AuthCookieAspNetCore";
                options.LoginPath = "/Login/Index";
                options.LogoutPath = "/Login/Logout";
                options.Cookie.Expiration = TimeSpan.FromMinutes(5);
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.None;
                options.Cookie.SameSite = SameSiteMode.Lax;

            });
            

            var _symmetricSecurityKey = new SymmetricSecurityKey(
                   Convert.FromBase64String(ObjConfiguracion.JWT_SECRET_KEY)
               );

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)

           .AddJwtBearer(options =>
           {
               options.SaveToken = true;
               options.TokenValidationParameters = new TokenValidationParameters
               {

                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = false,
                   ValidIssuer = ObjConfiguracion.JWT_ISSUER_TOKEN,
                   ValidAudience = ObjConfiguracion.JWT_AUDIENCE_TOKEN,
                   IssuerSigningKey = _symmetricSecurityKey
               };
               IdentityModelEventSource.ShowPII = true;
               options.Events = new JwtBearerEvents
               {
                   OnTokenValidated = context =>
                   {
                       // Add the access_token as a claim, as we may actually need it
                       var accessToken = context.SecurityToken as JwtSecurityToken;
                       if (accessToken != null)
                       {
                           ClaimsIdentity identity = context.Principal.Identity as ClaimsIdentity;
                           if (identity != null)
                           {
                               context.HttpContext.Session.SetString("JWToken", accessToken.RawData
                                   );
                               identity.AddClaim(new Claim("access_token", accessToken.RawData));
                           }
                       }

                       return Task.CompletedTask;
                   },
                   OnAuthenticationFailed = context =>
                   {
                       var invalidAudienceException = context.Exception as SecurityTokenInvalidAudienceException;
                       context.Fail(context.Exception.Message.ToString());


                       return Task.CompletedTask;
                   }
               };
           });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSession();
            app.Use(async (context, next) =>
            {
                var JWToken = context.Session.GetString("JWToken");
                if (!string.IsNullOrEmpty(JWToken))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + JWToken);
                }
                await next();
            });
            app.UseCors(builder => builder.AllowAnyOrigin()
                               .AllowAnyMethod()
                               .AllowAnyHeader());
            app.UseHttpsRedirection();
           
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
           
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

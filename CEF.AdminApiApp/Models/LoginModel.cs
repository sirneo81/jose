﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.AdminApiApp.Models
{
    public class LoginModel
    {
        [DisplayName("Usuario CEF")]
        public string User { get; set; }
        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

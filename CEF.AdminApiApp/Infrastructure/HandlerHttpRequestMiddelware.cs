﻿using CEF.ServicesAuth.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEF.AdminApiApp.Infrastructure
{
    public class HandlerHttpRequestMiddelware
    {
        private readonly RequestDelegate _next;
        private readonly ILog _Logg;
        public HandlerHttpRequestMiddelware(RequestDelegate next, ILog Logg)
        {
            _next = next;
            _Logg = Logg;
        }
        public async Task Invoke(HttpContext context, Func<Task> next)
        {
            var req = context.Request;
            var bodyStr = "";
            // Allows using several time the stream in ASP.Net Core
            try { req.EnableRewind(); } catch { }

            // Arguments: Stream, Encoding, detect encoding, buffer size 
            // AND, the most important: keep stream opened
            using (StreamReader reader
                      = new StreamReader(req.Body, Encoding.UTF8, true, 1024, true))
            {
                bodyStr = reader.ReadToEnd();
            }

            // Rewind, so the core is not lost when it looks the body for the request
            req.Body.Position = 0;
            await next.Invoke();
        }
    }
    
}

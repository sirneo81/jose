﻿using CEF.EFAuthJwt.IEF;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.EFAuthJwt.EF
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private EFDataContext _repositoryContext;
        private IRepository<T_AppKeys> _apps { get; set; }
        private IRepository<T_Users> _t_users { get; set; }
        private IRepository<T_Roles> _roles { get; set; }
        public IRepository<T_AppKeys> Apps
        {
            get
            {
                if (_apps == null)
                {
                    _apps = new Repository<T_AppKeys>(_repositoryContext);
                }

                return _apps;
            }
        }
        public IRepository<T_Roles> Roles
        {
            get
            {
                if (_roles == null)
                {
                    _roles = new Repository<T_Roles>(_repositoryContext);
                }

                return _roles;
            }
        }
        public IRepository<T_Users> Users
        {
            get
            {
                if (_t_users == null)
                {
                    _t_users = new Repository<T_Users>(_repositoryContext);
                }

                return _t_users;
            }
        }
        public RepositoryWrapper(EFDataContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }
        public RepositoryWrapper(String ConecctionString)
        {
            _repositoryContext = new EFDataContext(ConecctionString);
        }
        public RepositoryWrapper( )
        {
             
        }
        public void save()
        {
            _repositoryContext.SaveChanges();
        }
        public bool CanConnect()
        {
            return _repositoryContext.Database.CanConnect();
        }

    }
}

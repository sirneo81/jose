﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CEF.EFAuthJwt.EF
{
    [Table("T_AppKeys")]
    public class T_AppKeys
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [StringLength(150)]
        public string Name { set; get; }
         
        public bool Actived { set; get; }
        [StringLength(500)]
        public string Token { set; get; }
        [StringLength(250)]
        public string Code { set; get; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using CEF.EFAuthJwt.IEF;
using System.Linq;
namespace CEF.EFAuthJwt.EF
{
    public class Repository<T> : IRepository<T> where T : class
    {
        internal EFDataContext ObjContext;
        internal int nmax = 50;
        private readonly DbSet<T> dbSet;
        public Repository(EFDataContext oContext)
        {

            ObjContext = oContext;
            ObjContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;


            dbSet = oContext.Set<T>();
        }

        public Task<List<T>> GetFilter(
        Expression<Func<T, bool>> filter = null, Func<IQueryable<T>,
        IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
            {
                return orderBy(query).ToListAsync<T>();
            }
            else
            {
                return query.ToListAsync<T>();
            }
        }
        public async Task<T> Get(int id)
        {
            var oEntity = await dbSet.FindAsync(id);
            ObjContext.Entry<T>(oEntity).State = EntityState.Detached;
            return oEntity;
        }
        public async Task SaveChanges()
        {
            await ObjContext.SaveChangesAsync();
        }
        public async Task<T> Addasync(T element)
        {
            dbSet.Add(element);
            await SaveChanges();
            return element;
        }
        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            SaveChanges();


        }
        public async Task DeleteAsync(int id)
        {
            T entity = await this.Get(id);
            Delete(entity);
        }

        public async Task<bool> Exist(int id)
        {
            bool existe = false;
            T entity = await this.Get(id);
            if (entity != null) existe = true;

            return existe;
        }


        public async Task<List<T>> GetAllReference(string aProperties = "")
        {

            IQueryable<T> query = dbSet;


            foreach (var lProperty in aProperties.Split
           (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(lProperty);
            }


            query = query.AsNoTracking();

            return await query.ToListAsync<T>();

        }
        public async Task<List<T>> GetAll()
        {
            var ObjResultado = from rs in dbSet

                               select rs;
            List<T> ObjLista = new List<T>();
            if (ObjResultado.Count() > 0)
            {
                ObjLista = ObjResultado.ToList<T>();

            }
            return ObjLista;
        }

        public void Update(T element)
        {
            dbSet.Update(element);

            ObjContext.SaveChanges();
        }


        public async Task<List<T>> GetFilterasync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.AsNoTracking().Where(filter);

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync<T>();
            }
            else
            {
                return await query.ToListAsync<T>();
            }
        }

        public Task<T> Getsync(int id)
        {
            throw new NotImplementedException();
        }

        public T GetsyncId(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.AsNoTracking().Where(filter);

            if (orderBy != null)
            {
                return orderBy(query).FirstOrDefault<T>();
            }
            else
            {
                return query.FirstOrDefault<T>();
            }
        }

        public async Task UpdateAsync(T element)
        {
            dbSet.Update(element);

            await SaveChanges();
        }

        public async Task<List<T>> GetFilterasyncReference(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string aProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.AsNoTracking().Where(filter);
            foreach (var lProperty in aProperties.Split
           (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(lProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync<T>();
            }
            else
            {
                return await query.ToListAsync<T>();
            }
        }
    }
}

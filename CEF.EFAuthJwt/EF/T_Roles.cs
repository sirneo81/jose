﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CEF.EFAuthJwt.EF
{
    [Table("T_Roles")]
    public class T_Roles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [StringLength(50)]
        public string NombreRol { set; get; }
        [StringLength(20)]
        public string NombreCorto { set; get; }
        public ICollection<T_Users> Users { get; set; }

    }
}

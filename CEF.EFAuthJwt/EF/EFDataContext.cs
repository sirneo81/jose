﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.EFAuthJwt.EF
{
   public class EFDataContext: DbContext
    {
        private string s_connectionString;
        public DbSet<T_AppKeys> T_RegisterMooney_set { get; set; }
        public DbSet<T_Users> T_Users_set { get; set; }
        public DbSet<T_Roles> T_Roles_set { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(s_connectionString);
            }
            else
            {



            }

            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<T_Roles>().HasMany(x => x.Users ).WithOne(y => y.Rol).HasForeignKey(x => x.IdRol);
            builder.Entity<T_Users>().HasOne(x => x.Rol).WithMany(y => y.Users);
            
            base.OnModelCreating(builder);

            s_connectionString = this.Database.GetDbConnection().ConnectionString;
            // builder.Entity<T_Traking>()
            //      .HasOne(p => p.T_Vehicle)
            //        .WithMany(b => b.Trakings).HasForeignKey(p => p.IdVehicles).HasPrincipalKey(p => p.Id);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public EFDataContext(DbContextOptions<EFDataContext> opt) : base(opt)
        {







        }
        public EFDataContext(string conenecionString) : base()
        {
            s_connectionString = conenecionString;
        }
    }
}

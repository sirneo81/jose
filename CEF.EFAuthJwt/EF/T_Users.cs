﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CEF.EFAuthJwt.EF
{
    [Table("T_Users")]
    public class T_Users
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [StringLength(50)]
        public string Name { set; get; }
        [StringLength(250)]
        public string SecondName { set; get; }
        [StringLength(50)]
        public string Login { set; get; }
        [StringLength(50)]
        public string Password { set; get; }

        public bool Actived { set; get; }
        [StringLength(250)]
        public string CodeAuth { set; get; }
        public int IdRol { get; set; }
        public T_Roles Rol { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace CEF.EFAuthJwt.IEF
{
    public interface IRepository<T> where T : class
    {
        Task<bool> Exist(int id);
        Task<List<T>> GetAll();
        Task<List<T>> GetAllReference(string aProperties = "");
        Task<List<T>> GetFilterasyncReference(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string aProperties = "");

        Task<List<T>> GetFilterasync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task<List<T>> GetFilter(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task<T> Get(int id);
        Task<T> Getsync(Int32 id);
        T GetsyncId(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
        Task DeleteAsync(int id);

        Task UpdateAsync(T element);
        void Update(T element);
        Task<T> Addasync(T element);

    }
}

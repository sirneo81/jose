﻿using CEF.EFAuthJwt.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.EFAuthJwt.IEF
{
    
        public interface IRepositoryWrapper
        {
            IRepository<T_AppKeys> Apps { get; }
        IRepository<T_Users> Users { get; }
        IRepository<T_Roles> Roles { get; }
        void save();
            bool CanConnect();
        }
    
}

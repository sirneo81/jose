﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CEF.ServicesAppAdminApi.Model
{
    public class UserApp
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public string SecondName { set; get; }

        [DisplayName("Usuario")]
        public string Login { set; get; }

        [DisplayName("Contraseña")]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        public bool Actived { set; get; }

        public string CodeAuth { set; get; }
         
        public Rol Rol { get; set; }
        public UserApp()
        {
            this.Name = "";
            this.SecondName = "";
            this.Login = "";
            this.Password = "";
            this.Actived = false;
            this.CodeAuth = "";
            this.Rol = new Rol();

        }
    }
}

﻿namespace CEF.ServicesAppAdminApi.Model
{
    public class Rol
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreCorto { get; set; }
    }
}
﻿using CEF.EFAuthJwt.EF;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAppAdminApi.IServices;
using CEF.ServicesAppAdminApi.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAppAdminApi.Services
{
    public class Roles : IRoles
    {
        internal IRepositoryWrapper _repository;
        public Roles(IRepositoryWrapper repository)
        {
            _repository = repository;
        }


        public async Task<Rol> AddRol(Rol o)
        {
            T_Roles ObjRol = UnMap(o);


            ObjRol= await _repository.Roles.Addasync(ObjRol);
            o.Id = ObjRol.Id;
            return o;
        }

        public async Task<bool> DelRol(int id)
        {
            await _repository.Roles.DeleteAsync(id);
            return false;
        }

        public async Task<Rol> GetRolId(int id)
        {
            Rol ObjUser = null;
            
            List<T_Roles> ObjResultado = await _repository.Roles.GetFilterasync(x => x.Id == id);
            if (ObjResultado.Count > 0)
            {
                return ObjUser = Map(ObjResultado[0]);
            }
            return ObjUser;
        }

        public async Task<List<Rol>> GetRols()
        {
            List<Rol> ObjRols = new List<Rol>();
            bool exist = false;
            List<T_Roles> ObjResultado = await _repository.Roles.GetAll();
            if (ObjResultado.Count > 0)
            {
                foreach (T_Roles item in ObjResultado)
                {
                    ObjRols.Add(Map(item));
                }


            }
            return ObjRols;
        }

        

        public async Task<bool> UpdateRol(Rol UpdateRol)
        {
            T_Roles ObjRol = UnMap(UpdateRol);


             await _repository.Roles.UpdateAsync(ObjRol);
            
            return true;
        }
        public static T_Roles UnMap(Rol o)
        {
            T_Roles e = new T_Roles();
            e.Id = o.Id;
            e.NombreRol = o.Nombre;
            e.NombreCorto = o.NombreCorto;
            return e;

        }
        public static Rol Map(T_Roles o)
        {
            Rol e = new Rol();
            e.Id = o.Id;
            e.Nombre = o.NombreRol;
            e.NombreCorto = o.NombreCorto;
            return e;

        }
    }
}

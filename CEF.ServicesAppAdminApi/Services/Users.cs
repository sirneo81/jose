﻿using CEF.EFAuthJwt.EF;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAppAdminApi.IServices;
using CEF.ServicesAppAdminApi.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAppAdminApi.Services
{
    public class Users : IUsers
    {
        internal IRepositoryWrapper _repository;
        public Users(IRepositoryWrapper repository)
        {
            _repository = repository;
        }
        public async Task<UserApp> GetUserById(int id)
        {
            UserApp oUser = new UserApp();
            List<T_Users> ObjResultado = await _repository.Users.GetFilterasync(x => x.Id == id);
            if (ObjResultado.Count > 0)
            {
                oUser = (MapOauthUser(ObjResultado[0]));
            }
            return oUser;
        }

        public async Task<UserApp> GetUserByUserName(string UserName)
        {
            UserApp oUser = new UserApp();
            List<T_Users> ObjResultado = await _repository.Users.GetFilterasync(x => x.Login.ToLower() == UserName.ToLower() && x.Actived );
            if (ObjResultado.Count > 0)
            {
                oUser=(MapOauthUser(ObjResultado[0]));
            }
            return oUser;
        }
        public async Task<List<UserApp>> GetUsuarios()
        {
            List<UserApp> ObjUsers = new List<UserApp>();

            bool exist = false;
            List<T_Users> ObjResultado = await _repository.Users.GetAll();
            if (ObjResultado.Count > 0)
            {
                foreach (T_Users item in ObjResultado)
                {
                    ObjUsers.Add(MapOauthUser(item));
                }


            }
            return ObjUsers;
        }
        public async Task<bool> VerifyUser(string UserName, string PassWord)
        {
            bool exist = false;
            List<T_Users> ObjResultado = await _repository.Users .GetFilterasync(x => x.Login.ToLower() == UserName.ToLower() && x.Actived &&
            x.Password == PassWord );
            if (ObjResultado.Count > 0)
            {
                exist = true;
            }
            return exist;
        }
        public static T_Users UpMapOauthUser(UserApp o)
        {
            T_Users r = new T_Users();
            r.Id = o.Id;
            r.Name = o.Name;
            r.Login = o.Login;
            r.SecondName = o.SecondName;
            r.Actived = o.Actived;
            r.CodeAuth = o.CodeAuth;

            return r;

        }
        public static UserApp MapOauthUser(T_Users o)
        {
            UserApp r = new UserApp();
            r.Id = o.Id;
            r.Name = o.Name;
            r.SecondName = o.SecondName;
            r.Login = o.Login;
            r.Password = "********";
            r.Actived = o.Actived;
            r.CodeAuth = o.CodeAuth;
            return r;

        }
    }
}

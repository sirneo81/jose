﻿using CEF.ServicesAppAdminApi.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAppAdminApi.IServices
{
    public interface IRoles
    {
        Task<List<Rol>> GetRols();
        Task<Rol> GetRolId(int id);
        
  
        Task<bool> DelRol(int id);
        Task<bool> UpdateRol(Rol UpdateRol);
       
        Task<Rol> AddRol(Rol o);
         
      
    }
}

﻿using CEF.ServicesAppAdminApi.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAppAdminApi.IServices
{
    public interface IUsers
    {
        Task<UserApp> GetUserById(int id);
        Task<UserApp> GetUserByUserName(String UserName);
        Task<bool > VerifyUser(String UserName, String PassWord);
        Task<List<UserApp>> GetUsuarios();
    }
}

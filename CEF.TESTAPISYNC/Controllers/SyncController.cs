﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CEF.TESTAPISYNC.Models;
using Microsoft.AspNetCore.Mvc;

namespace CEF.TESTAPISYNC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyncController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public CEFResponse Get( )
        {
            SyncReponse ObjRespuesta = new SyncReponse();
            ObjRespuesta.EntityName = "Prueba";
            ObjRespuesta.Result = 10;
            ObjRespuesta.StatusMessage = "Accept";
            ObjRespuesta.Synced = true;
            CEFResponse ObjRespuestaControlado = new CEFResponse();
            ObjRespuestaControlado.FaltMessage = "Prueva2";
            ObjRespuestaControlado.isFaulted = false;
            ObjRespuestaControlado.result = ObjRespuesta;
            return ObjRespuestaControlado;
        }
        [HttpPost]
        public CEFResponse Post(CEFRequest value)
        {
            SyncReponse ObjRespuesta= new SyncReponse();
            ObjRespuesta.EntityName = "Prueba";
            ObjRespuesta.Result = 10;
            ObjRespuesta.StatusMessage = "Accept";
            ObjRespuesta.Synced = true;
            CEFResponse ObjRespuestaControlado = new CEFResponse();
            ObjRespuestaControlado.FaltMessage = "Prueva2";
            ObjRespuestaControlado.isFaulted = false;
            ObjRespuestaControlado.result = ObjRespuesta;
            return ObjRespuestaControlado;
        }
 
    }
}

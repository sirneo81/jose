﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.TESTAPISYNC.Models
{
    public class CEFRequest
    {
        /// <summary>
        /// Id del registro de Syncro
        /// </summary>
        public string syncroId { get; set; }

        /// <summary>
        /// Id de la entidad principal
        /// </summary>
        public string entityId { get; set; }

        /// <summary>
        /// Nombre de la entidad
        /// </summary>
        public string entityName { get; set; }

        /// <summary>
        /// Objeto ERP
        /// </summary>
        public string erpObjectName { get; set; }


        /// <summary>
        /// Id de la acción
        /// </summary>
        public int actionId { get; set; }

        /// <summary>
        /// Id de usuario
        /// </summary>
        public string userId { get; set; }

        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string userName { get; set; }

        /// <summary>
        /// Nombre de la acción
        /// </summary>
        public string actionName { get; set; }


        /// <summary>
        /// Id de la segunda entidad en el caso de que sea un merge
        /// </summary>
        public string SecundaryEntityId { get; set; }

        /// <summary>
        /// Nombre de la segunda entidad en el caso de que sea un merge
        /// </summary>
        public string SecundaryEntityName { get; set; }

        /// <summary>
        /// Indica si la operación es Síncrona
        /// </summary>
        public bool Synchronous { get; set; }

    }
}

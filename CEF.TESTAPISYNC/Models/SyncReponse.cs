﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.TESTAPISYNC.Models
{
    public class SyncReponse
    {
        public bool Synced { get; set; }

        /// <summary>
        /// Nombre de la entidad
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Mensaje de estado
        /// </summary>
        public string StatusMessage { get; set; }

        public int Result { get; set; }

        public SyncReponse()
        {
            Synced = false;
        }

    }
}

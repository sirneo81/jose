﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.TESTAPISYNC.Models
{
    public class CEFResponse
    {
        public bool isFaulted { get; set; }
        public string FaltMessage { get; set; }
        public SyncReponse result { get; set; }
    }
}

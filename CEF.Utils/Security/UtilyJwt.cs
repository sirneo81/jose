﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CEF.Utils.Security
{
    public class UtilyJwt  
    {
        public static object SecurityAlgorithms { get; private set; }
        public    bool ValidarTokenJwt(string token, string JWT_SECRET_KEY, string JWT_AUDIENCE_TOKEN, string JWT_ISSUER_TOKEN,
            string JWT_EXPIRE_MINUTES)
        {
            var secretKey = JWT_SECRET_KEY;
            var audienceToken = JWT_AUDIENCE_TOKEN;
            var issuerToken = JWT_ISSUER_TOKEN;
            var expireTime = JWT_EXPIRE_MINUTES;
            string privateKey = JWT_SECRET_KEY;

            var _symmetricSecurityKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(privateKey)
              );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, "HS256"
                );
            var validationParameter = new TokenValidationParameters()
            {

                ValidAudience = audienceToken,
                ValidIssuer = JWT_ISSUER_TOKEN,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = false,
                ValidateLifetime = true,
            };

            try
            {
                var handler = new JwtSecurityTokenHandler();
                ClaimsPrincipal ObjClaims = handler.ValidateToken(token, validationParameter, out var tokens);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                //if (ex.Message.Contains("signature"))
                //{
                //    return false;
                //}
                //else { return false; }
            }
        }
        public static   string GenerateTokenJwt(string username, string JWT_SECRET_KEY, string JWT_AUDIENCE_TOKEN, string JWT_ISSUER_TOKEN, 
            string JWT_EXPIRE_MINUTES , List<Claim> ObjListaClaims)
        {
            var secretKey = JWT_SECRET_KEY;
            var audienceToken = JWT_AUDIENCE_TOKEN;
            var issuerToken = JWT_ISSUER_TOKEN;
            var expireTime = JWT_EXPIRE_MINUTES;
            string privateKey = JWT_SECRET_KEY;

            var _symmetricSecurityKey = new SymmetricSecurityKey(
                  Convert.FromBase64String(privateKey)
              );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, "HS256"
                );


            ClaimsIdentity claimsIdentity = new ClaimsIdentity(ObjListaClaims);


            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(int.Parse(JWT_EXPIRE_MINUTES)),// DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                signingCredentials: _signingCredentials);

            string jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);

            return jwtTokenString;
        }

    }
}

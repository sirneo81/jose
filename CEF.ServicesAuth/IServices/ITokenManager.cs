﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.IServices
{
    interface ITokenManager
    {
        Task<bool>  SaveToken(string id, string Token);
        Task<string> GetToken(string id);
       
    }
}

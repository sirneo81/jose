﻿using CEF.ServicesAuth.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.IServices
{
    public interface IApplicationKey
    {
        Task<AppToken> GetTokenFromAppKey(int id);
        Task<AppToken> SaveTokenToApp(AppToken o);
        Task<List<AppToken>> GetApps();
        Task<AppToken> GetTokenFromAppKeyCode(string Code);

    }
}

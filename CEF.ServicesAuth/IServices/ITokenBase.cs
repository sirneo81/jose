﻿ 
using CEF.ServicesAuth.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.IServices
{
   public  interface ITokenBase
    {
        Task<string> GenerateTokenJwt(string username, CEFConfigurationApiAuth Configuration, List<Claim> ObjListaClaims);
        Task<bool> ValidarTokenJwt(string token, CEFConfigurationApiAuth Configuration);
    }
}

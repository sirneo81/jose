﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.IServices
{
    public interface ILog
    {
        Task LogAddError(string mensaje);
        Task LogAddInfo(string mensaje);
        Task LogAddTrace(string mensaje);
        Task<bool> CreateNewContainer();
        Task<bool> DeleteContainer();
    }
}

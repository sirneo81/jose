﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.ServicesAuth.Model
{
    public class LogConfigurationFile
    {
      
        public string PATH = "";
        public string PATTERN_FILE = "";
        public bool Activar = false;
        public LogConfigurationFile(IConfiguration Configuracion)
        {
            PATH = Configuracion.GetValue<string>("PATHLOG");
            PATTERN_FILE = Configuracion.GetValue<string>("PATTERNFILELOG");
            Activar = Configuracion.GetValue<bool>("LogFile:Activar");
        }
        public LogConfigurationFile() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.ServicesAuth.Model
{
    public static class AppRoles
    {
        public static string Admin = "Administrator";
        public static string Read = "ReadOnly";
    }
}

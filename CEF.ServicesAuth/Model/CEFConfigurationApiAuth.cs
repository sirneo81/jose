﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.Model
{
    public class CEFConfigurationApiAuth
    {
        public string JWT_SECRET_KEY = "";
        public string JWT_AUDIENCE_TOKEN = "";
        public string JWT_ISSUER_TOKEN = "";
        public string JWT_EXPIRE_MINUTES = "";
        public string ConnectionStringDB = "";
        public CEFConfigurationApiAuth(IConfiguration Configuracion)
        {
            JWT_SECRET_KEY = Configuracion.GetValue<string>("JWTSECRETKEY");
            JWT_AUDIENCE_TOKEN = Configuracion.GetValue<string>("JWTAUDIENCETOKEN");
            JWT_ISSUER_TOKEN = Configuracion.GetValue<string>("JWTISSUERTOKEN");
            JWT_EXPIRE_MINUTES = Configuracion.GetValue<string>("AppSettings:JWT_EXPIRE_MINUTES");
            ConnectionStringDB = Configuracion.GetValue<string>("ConnectionStringDB");
        }
        public CEFConfigurationApiAuth() { }
    }
}

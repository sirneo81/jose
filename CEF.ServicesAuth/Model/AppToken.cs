﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEF.ServicesAuth.Model
{
    public  class AppToken
    {
        public string Token { get; set; }
        public int IdApp
        {
            get;set;
        }
        public bool Actived { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}

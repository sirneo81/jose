﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
 
using CEF.ServicesAuth.Model;
using CEF.ServicesAuth.IServices;
using Microsoft.IdentityModel.Tokens;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.Services;

namespace CEF.ServicesAuth.Services
{
    public class TokenManager : ITokenBase, ITokenManager
    {
        private IApplicationKey _appDB;
        internal IRepositoryWrapper _repo;

        public TokenManager(IRepositoryWrapper Repo)
        {
            _repo = Repo;
            _appDB = new ApplicationKey(Repo);
        }
        
        public async Task<string> GenerateTokenJwt(string username, CEFConfigurationApiAuth Configuration, List<Claim> ObjListaClaims)
        {
            var secretKey = Configuration.JWT_SECRET_KEY;
            var audienceToken = Configuration.JWT_AUDIENCE_TOKEN;
            var issuerToken = Configuration.JWT_ISSUER_TOKEN;
            var expireTime = Configuration.JWT_EXPIRE_MINUTES;
            string privateKey = Configuration.JWT_SECRET_KEY;

            var _symmetricSecurityKey = new SymmetricSecurityKey(
                  Convert.FromBase64String(privateKey)
              );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );


            ClaimsIdentity claimsIdentity = new ClaimsIdentity(ObjListaClaims);


            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(int.Parse(Configuration.JWT_EXPIRE_MINUTES)),// DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                signingCredentials: _signingCredentials);

            string jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);

            return jwtTokenString;
        }

        public async Task<string> GetToken(string id)
        {
            AppToken o = await _appDB.GetTokenFromAppKey(int.Parse(id));
            return o.Token;
        }

        public async  Task<bool> SaveToken(string id, string Token)
        {
            AppToken o =await  _appDB.GetTokenFromAppKey(int.Parse(id));
            o.Token = Token;
            await _appDB.SaveTokenToApp(o);
            return true;
        }

        public async Task<bool> ValidarTokenJwt(string token, CEFConfigurationApiAuth Configuration)
        {
            var secretKey = Configuration.JWT_SECRET_KEY;
            var audienceToken = Configuration.JWT_AUDIENCE_TOKEN;
            var issuerToken = Configuration.JWT_ISSUER_TOKEN;
            var expireTime = Configuration.JWT_EXPIRE_MINUTES;
            string privateKey = Configuration.JWT_SECRET_KEY;

            var _symmetricSecurityKey = new SymmetricSecurityKey(
                  Encoding.UTF8.GetBytes(privateKey)
              );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var validationParameter = new TokenValidationParameters()
            {

                ValidAudience = audienceToken,
                ValidIssuer = Configuration.JWT_ISSUER_TOKEN,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = false,
                ValidateLifetime = true,
                         };

            try
            {
                var handler = new JwtSecurityTokenHandler();
                ClaimsPrincipal ObjClaims = handler.ValidateToken(token, validationParameter, out var tokens);

                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("signature"))
                {
                    return false;
                }
                else { return false; }
            }
        }
    }
}

﻿using CEF.ServicesAuth.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CEF.ServicesAuth.Model;
using System.IO;
namespace CEF.ServicesAuth.Services
{
    public class LogFile : ILog
    {

        private readonly string _File;
        private readonly string _Path;
        private readonly string _fileroot;
        private LogConfigurationFile configuracion;
        private bool CanUse = false;
        public LogFile(LogConfigurationFile _configuracion)
        {
            
            configuracion = _configuracion;
            _File = _configuracion.PATTERN_FILE;
            _Path = _configuracion.PATH;
            _fileroot = _Path + "\\" + _File + ".log";
            try
            {
                if (!Directory.Exists(_Path))
                {
                    Directory.CreateDirectory(_Path);
                }
                if (!File.Exists(_Path + "\\" + _File + ".log"))
                {

                    File.CreateText(_Path + "\\" + _File + ".log");
                }
            }
            catch (Exception ex) { }
            CanUse = true;
        }

        public Task<bool> CreateNewContainer()
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteContainer()
        {
            throw new NotImplementedException();
        }

        public Task LogAddError(string mensaje)
        {
            if(CanUse)
            { 
                string content = "Error" + "\t" + DateTime.Now.ToString() + "\t" + mensaje + mensaje + System.Environment.NewLine;
                File.AppendAllText(_fileroot, content);
            }
            return Task.CompletedTask;

        
        }

        public Task LogAddInfo(string mensaje)
        {
            if (configuracion.Activar && CanUse)
            {
                string content = "Info" + "\t" + DateTime.Now.ToString() + "\t" + mensaje + System.Environment.NewLine;
                File.AppendAllText(_fileroot, content);
            }
            return Task.CompletedTask;
        }

        public Task LogAddTrace(string mensaje)
        {
            if (configuracion.Activar && CanUse)
            {
                string content = "Trace" + "\t" + DateTime.Now.ToString() + "\t" + mensaje + mensaje + System.Environment.NewLine;
                File.AppendAllText(_fileroot, content);
            }
            return Task.CompletedTask;
        }
    }
}

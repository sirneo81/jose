﻿using CEF.EFAuthJwt.EF;
using CEF.EFAuthJwt.IEF;
using CEF.ServicesAuth.IServices;
using CEF.ServicesAuth.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CEF.ServicesAuth.Services
{
    public class ApplicationKey : IApplicationKey
    {
        internal IRepositoryWrapper _repository;
        public ApplicationKey(IRepositoryWrapper repository)
        {
            _repository = repository;
        }
       
        public async Task<AppToken> GetTokenFromAppKey(int id)
        {
            AppToken ObjApp = new AppToken();
            bool exist = false; 
            List<T_AppKeys> ObjResultado = await _repository.Apps.GetFilterasync(x => x.Id == id);
            if (ObjResultado.Count > 0)
            {
                return ObjApp = Map(ObjResultado[0]);
            }
            return ObjApp;
        }
        public async Task<AppToken> GetTokenFromAppKeyCode(string Code)
        {
            AppToken ObjApp = new AppToken();
            bool exist = false;
            List<T_AppKeys> ObjResultado = await _repository.Apps.GetFilterasync(x => x.Code  == Code);
            if (ObjResultado.Count > 0)
            {
                return ObjApp = Map(ObjResultado[0]);
            }
            return ObjApp;
        }
        public async Task<List<AppToken>> GetApps()
        {
            List<AppToken> ObjApps = new List<AppToken>();
            bool exist = false;
            List<T_AppKeys> ObjResultado = await _repository.Apps.GetAll();
            if (ObjResultado.Count > 0)
            {
             
                ObjApps.Add(Map(ObjResultado[0]));
            }
            return ObjApps;
        }
        public async Task<AppToken> SaveTokenToApp(AppToken o)
        {
            T_AppKeys oItem = UnMap(o);
            if (oItem.Id  > 0)
            {
                await _repository.Apps.UpdateAsync(oItem);

            }
            else
            {
                oItem= await _repository.Apps.Addasync(oItem);
                o.IdApp = oItem.Id;
            }
            return o;
        }


        public static AppToken Map(T_AppKeys o)
        {
            AppToken oNew = new AppToken();
            oNew.Actived = o.Actived;
            oNew.IdApp = o.Id;
            oNew.Name = o.Name;
            oNew.Token = o.Token;
            oNew.Code = o.Code;
            return oNew;

        }
        public static T_AppKeys UnMap(AppToken o)
        {
            T_AppKeys oNew = new T_AppKeys();
            oNew.Actived = o.Actived;
            oNew.Id = o.IdApp;
            oNew.Name = o.Name;
            oNew.Token = o.Token;
            oNew.Code = o.Code;
            return oNew;

        }
    }
}

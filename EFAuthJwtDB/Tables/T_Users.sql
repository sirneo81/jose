﻿CREATE TABLE [dbo].[T_Users]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [SecondName] NVARCHAR(250) NOT NULL, 
    [Login] NVARCHAR(50) NOT NULL, 
    [Password] NVARCHAR(50) NOT NULL, 
    [Actived] BIT NOT NULL, 
    [CodeAuth] NVARCHAR(250) NOT NULL,
	IdRol int not null,
)

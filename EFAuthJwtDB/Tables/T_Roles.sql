﻿CREATE TABLE [dbo].[T_Roles]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [NombreRol] NVARCHAR(50) NULL, 
    [NombreCorto] NVARCHAR(20) NULL
)
